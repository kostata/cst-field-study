\section{Field Study Description}

%Most previous studies of feature location were undertaken in a
%controlled lab setting where the tasks the participants performed were
%carefully crafted by the researchers and clear observations of
%developer behavior can be put into context via pre- and
%post-experiment questionairres. While such controlled experiments have
%uncovered depth of knowledge about feature location, including
%developer mental models and typical work flows, they fail to capture
%the bredth of feature location tasks and scenarios in the field. The
%field study in this paper attempts to connect what we previously have
%learned about feature location to observations of developer behavior
%in the field.

Our field study of programmers performing feature location is based on
two data sets: (1) the Blaze dataset consisting of all the IDE
interactions of 67 developers at ABB, Inc. over a period of about 2
months; (2) the Sando dataset consisting of 8,052 queries using the
Sando search tool by 596 unknown developers in the field that
downloaded the tool and issued more than one query\footnote{Sando
  usage data spanning from 05/2013 to 06/2014 was included in the
  dataset.}. Sando users that issued only one query were presumed to
have only tried the tool and not performed any serious feature
location, and therefore their interactions with the tool were removed
from this dataset. We leveraged these datasets to shed more light on
developer behavior during feature location, focusing mostly on
observations during, before, and after the interaction of developers
with code search tools.


\subsection{Data Collection Procedure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{figures/blaze-sando-usage.eps}
\caption{Usage characteristics for the Blaze dataset (top) and 
the Sando dataset (bottom).}
\label{fig:usage} 
\end{figure}

The Blaze dataset was collected from 67 volunteer developers at ABB,
Inc. who installed a Visual Studio interaction monitoring
extension\footnote{The interaction monitoring extension, called Blaze,
  is implemented by researchers at ABB, Inc. Its name is the reason we
  refer to this dataset as such.}. This extension monitors and logs
clicks and keypresses within the Visual Studio IDE, capturing
timestamped information indicating each IDE command that was invoked
by a developer. The data collection timespan differed for most
developers as some of them installed the data collection tool later
than others, some stopped collection data earlier than others, while a
third group stopped and resumed data collection after a several day
pause. All of the data was collected in the second half of 2013, and
it consists of an average of 25.6 days of activity per developer with
a standard deviation of 25.4 days. The usage characteristics of the
Blaze dataset, in terms of work-days per user, is shown in the top
part of Figure~\ref{fig:usage}.

The Sando dataset consists of data describing developer interaction
with the Sando open-source code search tool \citep{sando}, implemented as a plugin
to Visual Studio. Sando retrieves a set of program elements
(e.g., methods, field, classes) in response to a user supplied text
query. It uses information retrieval techniques, such as word
stemming, and the vector space model to rank retrieved results based
on relevance to the query. Sando is commonly downloaded from the
Visual Studio Gallery
Site\footnote{\texttt{http://visualstudiogallery.msdn.microsoft.com}},
which houses numerous Visual Studio extensions and tools. When
installing Sando, users are asked to participate in volunteer data
collection, and if they agree anonymous usage data is periodically
uploaded from their machines to Sando servers. The Sando dataset spans
13 months and consists of 596 users and 8,052 queries. The number of
queries for each of the users in the Sando dataset are shown in the
bottom part of Figure~\ref{fig:usage}. This figure shows that, in both
the Blaze and Sando datasets, while the usage quantities vary
significantly between users, neither dataset is dominated by few users
and therefore should allow for widely generalizable observations.

\subsection{Data Description}

%what exactly was collected?  what kinds of events are logged? what does a log look like?  

\subsubsection{Blaze Dataset}

The Blaze dataset contains most user clicks or keypresses in the
Visual Studio IDE. Certain events are condensed for performance
reasons,  to reduce both the impact of data collection on
Visual Studio response time as well as the space overhead of the
gathered logs. For instance, keypresses on the Visual Studio editor
window are logged only if they move the cursor down a line, ignoring
edit commands that do not cause a line break and horizontal movements
of the cursor. Certain user actions within Visual Studio are
impossible to record as the IDE did not allow us to register a
listener for that type of event, for instance closing or dragging and
dropping a window within the IDE. Despite this, the number of logged
events in the dataset is substantial, consisting of over 3.2 million
entries. Below is an illustrative listing of events in the Blaze
dataset where a developer started the debugger ({\tt Debug.Start}) ,
then moved the cursor on the editor window ({\tt View.File} and ({\tt
  View.OnChangeCaretLine}), stopped on a breakpoint ({\tt Debug.Debug
  Break Mode}), opened the call stack for a method ({\tt View.Call
  Stack}), and used the Find in Files tool to search the code base
({\tt View.Find and Replace}, {\tt Edit.FindinFiles}, and {\tt
  View.Find Results 1}).


\vskip 0.5cm

{\small
\begin{verbatim}
2013-11-18 14:50:23.000, Debug.Start
2013-11-18 14:50:23.000, View.File
2013-11-18 14:50:23.000, View.OnChangeCaretLine
2013-11-18 14:50:23.000, Debug.Debug Break Mode
2013-11-18 14:50:33.000, View.Call Stack
2013-11-18 14:51:08.000, View.Find and Replace
2013-11-18 14:51:08.000, Edit.FindinFiles
2013-11-18 14:51:08.000, View.Find Results 1
\end{verbatim}
}

\vskip 0.5cm
 
All developers whose actions were captured in the Blaze dataset had
access to similarly configured versions of Visual Studio with a few
exceptions. Some developers used JetBrains' ReSharper toolkit for
Visual Studio \citep{resharper}, which offers advanced program
navigation and UI capabilities. Developers also had access to a custom
program navigation tool from ABB, Inc. called Prodet, which allows
exploration of a code base by following the call graph. 

The developers had three code search tools available to them: Quick
Find, Find in Files, and Sando. Quick Find, launched using the Ctrl+F
key press, performs searches local to the currently open file in the
editor using string matching or optional regular expression
input. Find in Files also uses string matching or regular expressions,
but produces results across the entire code base by default. The
results produced by Find in Files are displayed in a separate window,
using one line to describe the file containing the match, the matching
line number, and a brief snippet of the code on that line. The matches
from files currently open in the editor appear first, with the line
number as the second order sorting term. Sando uses sophisticated
Information Retrieval algorithms, based on the Vector Space Model, to
index identifiers in the source code and retrieve relevant
results. The results are ordered according to their relevance (using
tf*idf scoring). A several line long snippet of code corresponding to
each result can be previewed in a popup window using a single
click. Double clicking on a Sando result opens it in the Visual Studio
editor. Based on the scope of the search, we consider Quick Find a
file-scope search tool, while we consider Find in Files and Sando
project-scope search tools.



\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{figures/sando-query-cdf.pdf}
\caption{A cumulative distribution function showing the number 
of queries issued by proportions of Sando's 596 users.}
\label{fig:sando-cdf} 
\end{figure}



\subsubsection{Sando Dataset}

As developers issue queries about their software projects, the Sando
code search tool records and collects anonymous and private data on
developer interactions with the tool. Sando provides instructions that
describes its use, displayed on install and when clicking a help
button. The Sando dataset contains several types of information about
each query, including its length and similarity to the previously
submitted query, the retrieved results, including their number and
type, and clicks on the retrieved results, including their rank and
similarity to the query. The distribution of the number of queries
that each Sando user issued, as a cumulative distribution function is
shown in Figure~\ref{fig:sando-cdf}. Below is a representative snippet
of the Sando dataset, describing a user opening a Visual Studio
solution, issuing a query, receiving 11 results, and clicking on the
5th result in the list.

\vskip 0.5cm

{\small
\begin{verbatim}
2013-10-24 10:06:41,094 Solution opened: SolutionHash=522943624
2013-10-24 10:10:09,845 Query submitted by user: QueryDescription=Plain; 
                        DiceCoefficientToPreviousQuery=0
2013-10-24 10:10:10,006 Sando returned results: NumberOfResults=11;
2013-10-24 10:11:31,268 User double-clicked a result: TypeOfResult=XmlElement; 
                        ResultLanguage=OtherLang; ResultScore=0.051; 
                        ResultRank=5
\end{verbatim}
}

\vskip 0.5cm

The Sando dataset contains little contextual information on the size
of the code base, the language its implemented in, or other activities
that the developer performs to locate features in the code outside of
code search. Such information is not gathered due to privacy concerns,
which could limit wider adoption of Sando, especially in the
industrial setting. These threats are mitigated, however, by the size
and diversity of the dataset which should produce statistically
relevant measurements of developer behavior when performing code
search in the field. A listing of all of the events used in the
Sando and Blaze dataset is provided in Appendix~\ref{the-appendix}.


\subsection{Data Analysis}

Our main goal of the field study was to investigate developer behavior in regard to feature location
as developers complete their normal daily activities.  
Specifically, we investigate frequency and types of code search tools used,
developer behavior before and after search, types of queries issued by developers, and complex feature location patterns.
%methodology for data analysis for each question
Since both the Blaze and Sando datasets are large, spanning millions of log messages, and
our interest is in understanding feature location developer behavior,
we need an approach for identifying these user behaviors in the
dataset. Simple pattern recognition, where certain sequences of clicks
represent particular developer behaviors, is difficult to perform in
the Blaze dataset because of the presence of ambiguous sub-patterns of
clicks, which can correspond to several types of behavior.  

The key strategy to gain insight into developer behavior from such
low-level logged event data is to use the time of a specific click as
an additional clarification parameter that can help resolve some of the
ambiguity in the dataset. Therefore, based on sets of key log messages
corresponding to relevant developer actions in time, our goal was to
extract feature location {\em sessions}, where a specific set of key
messages occur one or more times within a short, on the order of
seconds, timespan. Based on the choice of the key messages that we
use, we can identify various types of sessions, for instance, search
sessions, structured navigation sessions, or debugging sessions. Also,
by using only the log messages that belong to a specific tool we can
produce, for example, Sando sessions or Quick Find sessions.

To automatically extract feature location sessions in the Blaze
dataset, we use hierarchical agglomerative clustering \citep{IRbook}
of log events according to their time distribution. This clustering
algorithm produces a clustering tree, which can be cut at a variety of
places to produce the desired number of clusters. We use a natural
cut, which uses the ratio of time differences of the key messages, to
choose the best number of clusters, constrained to between a 30-second
to 5-minute interval. We used this interval to select a cut between
the clusters as it reflects general expectations of user search
behavior. This is to say that any two log messages that are less than
30 seconds apart are required to belong in the same session, while two
messages with a distance of over 5 minutes are assumed not to be in the
same session. If the distance is between these two limits, then the
algorithm automatically determines whether or not the two messages
should be grouped in the same session based on the distribution of other Sando clicks in a specific developer's dataset.


For instance, consider three clicks on the Sando code search tools at
some relative times: 0 seconds, 15 seconds and 300 seconds. The
clustering algorithm would definitely group the first two clicks,
which are 15 seconds apart into the same session as they are below the
30 second lower threshold. Since the time between the second and third
click is 285 seconds (or 4 min and 45 seconds), which is below the 5
minute upper threshold, they may or may not be grouped together
depending on the distribution of other Sando clicks in a specific
developer's dataset. If the ratio of 285/15 seconds is largest, then
the second and third click would belong to different sessions;
however, if a larger ratio between two clicks exists for this
developer, then they would be in the same session.

Extracting groups of related developer actions in the Sando dataset is
less difficult, as interactions with the tool are based on a query,
and the developer's issue of a new query can be used as a delimiter in
extracting related log events. While we often analyze the Sando
dataset at the query level, we also rely on lexical similarity between
query terms to group sets of queries into reformulation sequences.  In
the context of code search, such sequences represent the developer
interacting with the tool to fulfill a single information need. This
sometimes requires several queries, reformulating previously issued
queries until a desired program element is located.

%While we sometimes analyze the Sando
%dataset at the query level, we also use a hierarchical clustering
%algorithm with a natural cut, as in the Blaze dataset, to group
%queries that are near in time into sessions.

\subsection{Threats to Validity}

The goal of our field study was to improve on the realism, size and
scope of previous studies in feature location. However, the challenge
of a field study is in interpreting developer behaviors from low-level
log data. This challenge is a particular source of threats to validity
to the findings in this paper.

%construction (is this the right way of measuring this)
A significant threat to validity arises from our lack of knowledge of
the tasks the developers in our study were trying to accomplish.  For
instance, we believe that they were performing software maintenance,
though they could have easily engaged in ``greenfield'' software
development for some of the time. This threat is mitigated partially
in that we make no behavioral assumptions in our analysis and look
solely at the events generated by the developers to form our
conclusions.

%internal (strong relationships in the study)
Our analysis is largely based on clustering individual events into
high level behaviors. While the clustering approach we used was simple
and straightforward, a possible threat is that this mapping could be
inaccurate. To mitigate this threat, we hand analyzed a sample of the
clustered behaviors for validity and determined them to be reasonable.

%Our adaptation (i.e. reduction) of the model of complex feature
%location sessions (by Wang et al. (\cite{wangFl})) could have excluded
%many relevant sessions. While this threat is possible, it is unlikely
%as our model contained the most relevant parts mentioned by other
%researchers (e.g. \cite{KoTSE2006, robillard}). 

%external (applies to other groups)
Our results may not generalize due to several facts: (1) the Blaze
dataset reflected developers from one company; and (2) all of the
developers used Visual Studio with a few specific plugin extensions.
The size of our Blaze dataset, consisting of over 60 developers,
mitigates these concerns, as well as the fact that most IDEs support
plugins and many developers use at least some of them, making this 
a realistic scenario.


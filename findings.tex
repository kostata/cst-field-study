\section{Findings}

The findings of our study are intended to answer questions that would
improve our understanding of feature location, including (1) how often
developers use code search tools in general; (2) which tools are used,
how often, relative to each other; (3) the types of queries issued to
code search tools; and (4) the overall context following and preceding
code search.

We present our findings grouped into two categories: (1) findings on the use of
code search tools and (2) findings on complex feature location
sessions.  The first category focuses on examining developer use of
code search tools, which are influential in feature location and
commonly represent the first step developers take in performing
feature location. Findings from analyzing the Sando dataset are presented  in this category. 
Findings from analyzing the Blaze dataset also provide insights into
developer behavior when
performing a complex feature location task, which spans a significant
period of time and uses a variety of IDE modalities (e.g., search and
structured navigation).


\subsection{Findings on the Use of Code Search Tools}


\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{figures/cst-new-solution.pdf}
\caption{Percent of code bases queried with Sando within a specific
  time interval (in seconds) of opening.}
\label{fig:cst-new-solution} 
\end{figure}

 
\noindent
\textbf{Developers tend to issue project-scope queries within a few
  minutes after opening a new code base.}

\noindent
The Sando dataset tracks the open solution event in Visual Studio,
representing when a developer opens a new code base to work on. There
are many code bases that never receive a query, presumably because the
developer is already familiar with the code base. However, our
analysis shows that solutions that receive a Sando query tend to get
an initial query soon after the solution is opened. 

The Sando dataset
contains 1595 queried solutions. Figure~\ref{fig:cst-new-solution} shows the distribution
of time between a newly opened code base and  the initial query.  A
large proportion of solutions, more than 40\%, are queried within the
first 2 minutes after being opened in Visual Studio.

\vskip 0.5cm

\noindent
{\bf Developers use \underline{both} file-scope and project-scope
  code search tools frequently and consistently. }

\noindent
We differentiate two types of code search tools: project-scope and
file-scope. Project-scope tools, like Find in Files and Sando in the
Blaze dataset, are used to query the entire code base. They are
commonly used to find a starting point in discovering a particular
feature in the code, but can also have other uses, such as a lookup of
an known identifier in a code base. File-scope code search tools, such
as Quick Find (Ctrl+F) in Visual Studio, search only the currently open
file. This search modality is common across a number of applications
(e.g., browser or word processor) and is commonly used to navigate within
an open page of text. Since  it is possible in Visual Studio to produce
project-scope search results with the file-scope search tool (Quick
Find) by clicking the “Find All” drop down button, we explicitly
looked for such sessions and categorized them as project-scope search
as long as they performed at least one such search.

Based on our clustering of log events into sessions and subsequent identification of
sessions as search sessions, each developer in the Blaze study performed an average of 3.32 search
sessions per day. Out of these searches, there were 1.7 project-scope
search sessions per user per day, using either Find in Files or Sando,
and there was an average of 1.57 file scope searches that used Quick
Find. As a reference, there was an average of 7.95 editing
sessions~\footnote{Editing sessions were identified by applying the
  session clustering algorithm on editing events.} per developer per
day.

Figure~\ref{fig:cst-sessions} shows the overall number of search
sessions per day for each of the 67 developers in the Blaze study,
and the proportion of those sessions that used project-scope
vs. file-scope search tool(s). For instance, the first user (leftmost
bar in Figure~\ref{fig:cst-sessions}) performed a total of 3.4 search
sessions per day, with an average of 1.9 being project-scope
sessions. There were 4/67 users who did not use any search tools at
all.


\begin{figure}[t]
\centering
\includegraphics[width=0.75\textwidth]{figures/code-search-sessions.pdf}
\caption{Number of project-scope and file-scope search sessions per
  user in the Blaze dataset.}
\label{fig:cst-sessions} 
\end{figure}

Almost all developers (60/67) used a file-scope search tool at some
point during our data collection, and, similarly, a large proportion
of the developers used one of the two project-scope search tools
(50/67). Most (47/67) developers used both file-scope and project
scope tools, while 28/67 used a combination of the two during the same
code search session. 

Search sessions that combined file-scope and
project-scope search tools followed either an expanding scope or a
reducing scope pattern. In expanding scope, developers started with a
file-scope search that did not retrieve satisfactory results, and they
expanded the scope by issuing a project-scope search query. In
reducing scope searches, the developers started with a project-scope
search and used file-scope search only when they had found the file
containing the code of interest and needed to drill down further to
the statement level. There was a roughly equal number of expanding
and reducing scope search strategies observed in the Blaze study.

To investigate the consistency of usage of search tools over time,
Figure~\ref{fig:cst-timeline} displays a timeline of the use of search
tools for individual users and on average. The dashed lines in the
image represent the number of search tool usage sessions per day,
averaged over a 3-day window, for each individual developer, while the
red solid line denotes the average usage over all developers in the
Blaze dataset. The Sando dataset indicates a similar conclusion to the
Blaze dataset and thus is not shown.  The graph indicates a fairly
consistent usage of search tools on a daily basis, with a few outliers
who performed considerably more search at times.  There was an average
of 3.53 queries and 2.29 sessions per user per day.
%These users generated an average of 0.7 result clicks per day.

We investigated the choice of individual search tools over time per
developer to see if developers used the same tool consistently,
switched tools consistently, or used various tools throughout the
monitored time period.  Sando is a more advanced project-scope code
search tool than Find in Files: it uses a more sophisticated
information retrieval algorithm instead of simple string matching and
also ranks the retrieved results instead of returning a lengthy
list. Thus, one might expect that once developers tried using Sando,
they would replace Find in Files usage with this improved tool. Out of
the 39 developers in the Blaze study who used Sando, only 13 of 39
(one third) never used Find in Files after using Sando. However, 26 of
39 (two thirds) developers used both tools interchangeably. The most
likely explanation for this is Sando's limitation in indexing all
languages in the Visual Studio ecosystem, such as Visual Basic and
Javascript. Find in Files, on the other hand, works consistently on
all files. The remaining set of developers (28/67) used only Find in
Files, either because they were unaware of Sando or because they
preferred Find in Files.

To examine how effectively feature location tools were used by
developers, we used the Sando dataset, which differentiates types of
user behavior with a retrieved result set. If we consider
double-clicking on a result as an indicator of retrieval
effectiveness, 31\% (2505/8052) of the queries had at least one
click. The median rank of the highest double-clicked result in the
retrieved result set was 2, with a standard deviation of 11.9,
following an exponential distribution. This is indicative of the fact
that while most developers clicked on the first couple of results,
there were some developers who examined results deep in the
retrieved set.


\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{figures/code-search-timeline.pdf}
\caption{A timeline of code search tool usage in the Blaze
  dataset. Individual users are shown as dashed lines, while the
  average across all users is shown as a red line.}
\label{fig:cst-timeline} 
\end{figure}

\vskip 0.5cm

\noindent
\textbf{Developers perform lookup style searches fairly often.}

\noindent
The most frequent interaction prior to and following a click on a code
search tool was a click on the editor window, constituting well over
50\% of pre and post code search events.  While we did not capture the
actual editor selections from cut/copy due to the associated slow
downs for users, we monitored copy/cut and paste commands being
performed.  The logs show that 9\% of messages immediately following
the opening of a Quick Find window were a paste; 14\% of messages
immediately following Find in Files window opening were a paste; and
5\% of messages immediately preceding any Sando interaction were copy
and paste.  The use of copy-paste queries suggests that developers are
searching for exact parts of code, i.e., performing lookup searches.
However, they are not the only indicator of lookup style searches, as
FindinFiles and Quick Find automatically copy selected items into the
query window and those events are not monitored, as well as there are
queries that are typed directly by the developer that could also be
lookup style searches without using copy and paste commands.  Thus,
these percentages are a minimum estimate of lookup searches, and thus
indicate that lookup searches occur fairly often with all these search
tools.

%Considering this fairly
%skewed distribution of actions surrounding a code search as well as
%the fact that this behavior can be generated with a selection followed
%by a keypress, the copy and paste commands are relatively frequently
%performed before and after interactions with a code search tool.

%Developers performed a significant amount of searches for exact parts
%of code, as indicated by the high amount of copy-paste commands
%surrounding search tool usage (even though FindInFiles and Quick Find
%automatically copy selected terms into the query window). 

\vskip 0.5cm

\noindent
\textbf{NavigateTo, generally believed to be the most effective tool
  for performing lookup-style searches, was rarely used.}

\noindent
In Visual Studio, the NavigateTo tool uses simple string matching to
retrieve type names (e.g., classes), method names, file names, or
project names. This kind of tool is commonly available in many IDEs,
such as Eclipse where a similar tool is called OpenType. Since it only
indexes a selected group of items, NavigateTo/OpenType can be very
effective, producing very few false positive or false negative results
for a developer query.

In the Blaze dataset, we detected only two instances of developers using
NavigateTo, each by two separate developers. Both of these developers used
Sando and Quick Find fairly consistently, but neither of them used Find
in Files. Murphy et al.'s study of command usage in Eclipse found
similar usage characteristics for OpenType, ranking the tool as the
least used navigation command by developers using the Eclipse IDE
(\cite{murphy-eclipse}). The fact that not many developers used
NavigateTo indicates that likely many of them are not very familiar
with the tool; unfamiliarity has been reported as a problem with many IDE
features in \cite{Murphy-Hill-FSE12}. The fact that the two developers
used NavigateTo in one isolated instance, while using other search
tools both before and after, is perhaps indicative of the lack of
flexibility of this tool.


To investigate the amount of searches in the Sando dataset that
could have been answered by NavigateTo, we computed whether the query string
could be found verbatim within the name (e.g., method name or class name)
of a result opened in the Visual Studio editor, which is triggered by a
double click on a result in Sando. 1074/8052 (or only 13.34\%) of Sando
queries retrieved a result that exactly matched the query that was
provided by the user. 
%NavigateTo, which only indexes method, class,
%file and project names, is likely a better choice for such name-based
%lookup-style searches, because it only indexes type definitions, and
%not individual lines of code, which would reduce irrelevant results in the
%retrieved set.

\vskip 0.5cm

\noindent
\textbf{Most project-scope queries consist of one plain word term.}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{figures/code-search-query-terms.eps}
\caption{Distribution of the number of terms per query in the Sando dataset (left). Number
of terms difference between an initial and a reformulated Sando query (right). }
\label{fig:cst-query-terms} 
\end{figure}

\noindent

The left graph in Figure~\ref{fig:cst-query-terms} presents the
distribution of number of terms per query in the Sando dataset.  The
graph shows that when searching code, developers who used Sando for
code search overwhelmingly issued simple, single plain term
queries. We define a single plain term as a space-delimited sequence
of characters that does not contain any punctuation symbols or a
camelcase pattern (i.e. a lower case followed by an upper case
letter). This query writing behavior is similar to that of Internet
search engine users, where one term queries are also the norm
(\cite{modern-ir-book}). One explanation for this behavior is provided
by the berry-picking model for Internet search, which suggests that
searchers often issue a shorter general query followed by a refinement
of that query by adding more terms if the initial result set was
unsatisfactory (\cite{berry-picking}). Another explanation is that
users are searching for a known place of the code, that they have
visited before, and therefore are constructing a specific query that
is unlikely to yield many ambiguous results (\cite{modern-ir-book}).
In certain cases, code search tool researchers have assumed that
developers issue longer queries than what we have observed in the
field. This is exemplified by the frequent use of commit messages as a
proxy for queries when researchers evaluate code search tools
(\cite{tracelab}), even though they are commonly sentence-like in
length and structure.




\vskip 0.5cm

\noindent
\textbf{About one in ten project-scope queries were part of a query
  reformulation sequence, where the developer usually followed a
  strategy of adding a single term.}

\noindent
To investigate query reformulation behavior, we examined the Sando
dataset to identify query sequences where the user reformulated a
previous query by adding or removing terms.  We used a word similarity
measurement between consecutive queries, where we computed the number
of words that were similar in the two queries.  Out of the 8,052
queries, 672 (or 8.35\%) were part of a reformulation query
sequence. Most of the query reformulation sequences were short,
consisting of one reformulated query while a few reformulation
sequences extended to 4-5 query reformulations.

The right graph in Figure~\ref{fig:cst-query-terms} shows the distribution of the number of terms difference between
an initial and a reformulated Sando query.
A larger proportion of reformulated
queries followed a strategy of increasing the number of terms in the
query than reducing or maintaining the same number of terms, 
most often by one more term.
However, there was still a number
of reformulated query sequences where one term was modified (88),
leaving the total number of terms in the query unchanged.


\subsection{Findings on Complex Feature Location Sessions}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{figures/wang-model.pdf}
\caption{The feature location model proposed by Wang et al., adapted
  to field data by removing ambiguous paths through the model.}
\label{fig:wang-model} 
\end{figure}

There is no single feature location tool available to software
developers, so developers often exhibit complex patterns of tool usage
when locating features in the code, commonly relying on combinations
of code search, structured navigation and debugging tools in the
IDE. To discover complex feature location sessions that incorporate a
variety of IDE tools and last a significant amount of time, we
leverage a feature location mental model constructed by Wang et
al.  in a controlled laboratory setting (\cite{wangFl}). This mental
model is detailed enough to be used as a basis for the interpretation
of the data we collected in the Blaze study. The model includes four
different stages: {\em Search}, {\em Extend}, {\em Validate}, and {\em
  Document}. During the {\em Search} phase, the developer locates an
initial point in the source code. The developer explores the
surroundings of that initial point by exploring the call graph using
static or dynamic means in the {\em Extend} phase, building an
understanding of the code feature, which can be confirmed during the
{\em Validate} phase. The {\em Document} phase records in writing the
results of feature location. This phase is not crucial to the model
and irrelevant to the field data that we 
analyze. 

Figure~\ref{fig:wang-model} shows an adaptation of Wang et al.'s model
to field data that takes into account, and removes, feature location
behavior that is ambiguous and may confound analysis (e.g., debugging
could be used to fix bugs or for comprehension so we remove sessions
that use debugging in the {\em Search} phase).   We use this adapted model to
classify feature location behavior in the field, while
trading off the ability to recognize certain feature location patterns
that are more ambiguous and difficult to determine.

A developer may or may not progress through all of the model's stages
for any feature location task. For instance, simple lookup-type
feature location can usually be accomplished using only a single,
rapid code search tool interaction. However, the complex exploratory
tasks that we are interested in here require progressing to several stages
in Wang et al.'s model, which in turn may require using several
different IDE action modes, including searching, navigation and
debugging. 

We analyzed our Blaze dataset for such complex sessions,
extracted using the previously used hierarchical agglomerative
clustering algorithm, constrained to last for longer than one
minute. We explored the extracted sessions to investigate recurring
patterns in how these complex feature location tasks were performed by
the developers in the Blaze dataset.


Our analysis produced 206 complex feature location sessions by 33 of
the original 67 developers in the Blaze dataset. 
Sessions that
proceeded past the {\em Search} phase were deemed complex in this
analysis. The majority of such sessions 196/206 (95\%) proceeded to
the {\em Extend} phase of the feature location model in
Figure~\ref{fig:wang-model}. Only 10/206 (5\%) of these sessions
proceeded to the {\em Validation} phase. 

\vskip 0.5cm

\noindent
\textbf{Structured navigation is the most common {\em Extend} phase strategy during feature location.}

\begin{table}
\caption{Distribution of navigation commands in the {\em Extend} phase of feature location sessions.}
\label{tab:nav-types}      
\begin{tabular}{lll}
\hline\noalign{\smallskip}
Command & Number of Occurrences & Percent of Occurrences \\
\noalign{\smallskip}\hline\noalign{\smallskip}
Go To Definition & 315 & 47\% \\
$^{*}$Prodet Call Graph Navigator & 202 & 30\% \\
Find All References & 115 & 17\%\\
ReSharper Find Usages & 27 & 4\% \\
\noalign{\smallskip}\hline
\end{tabular}
\end{table}

\noindent
Structured navigation, including viewing the call hierarchy of a
method, viewing the type hierarchy of a class, and navigating to the
definition of a method or class from their use, is the most frequent
strategy taken by developers following a search in the analyzed
complex feature location sessions. There were 190/196 (97\%) sessions
that used navigation, while the remaining 6/196 (3\%) sessions relied
on debugging in the {\em Extend} feature location phase.

We further analyzed the distribution of the kinds of navigation
commands employed by the developers. Table~\ref{tab:nav-types} shows
the results for commands with more than 1\% of overall occurrences,
indicating that {\tt Go To Definition}, which navigates from a use to
a definition of a program element, gets the most use. The {\tt Prodet
  Call Graph Navigator}, a call graph exploration tool proprietary to
ABB, Inc., is second most frequent. However, its popularity relative
to other navigation commands is deceptive, since exploring a call
graph in Prodet often requires several clicks for a single session.
{\tt Find All References} is also a popular IDE command that displays
a list of uses for a specific identifier. Finally, the {\tt ReSharper
  Find Usages} command is the {\tt Find All References} command
equivalent in the popular ReSharper toolkit for Visual Studio.


\vskip 0.5cm
 
\noindent
\textbf{Developers in complex feature location sessions often switched between two
different tool modalities.}

A number of complex feature location sessions (41/206 or 20\%)
exhibited repetitive use of several tools and commands from distinct
modalities (i.e. code search, structured navigation, debugging). For
instance, there were 35 sessions where the developers frequently
alternated between project-scope search and structured navigation and
6 sessions where the developers alternated between search and
debugging. The alternating pattern was significant, consisting of an
average of 2.9 repetitions of search and navigations and 3.6
repetitions of searching and debugging. 

One possible reason for these
patterns is that the developer realizes after entering
the {\em Extend} phase of feature location that the {\em Search} phase
was inadequate. Another possibility is that since the search,
navigation, and debugging tools do not share information, the developer
has to repeat previous steps when forgetting specifics about the
current feature location task.




\section{Introduction}
Many studies of developers performing software maintenance and
evolution tasks have highlighted the challenges of performing
effective feature location \citep{wangFl,KoTSE2006}, which
constitutes identifying the parts of the code that implement a
specific functionality. Similar studies have also shown that poor
feature location can affect both the speed with which the maintenance
task is completed as well as the quality of the resulting code changes
\citep{robillard}. While such controlled laboratory studies have
uncovered a great deal about feature location, they all observe
developers in synthetic circumstances where the code bases are
completely unfamiliar to the study participants while the time window
to perform each task is artificially limited. In the field, developers
commonly examine code at their own pace and possess some knowledge of
the code base, albeit incomplete when the scale of the project is
large enough. Conducting a field study of feature location, where
software developers are monitored during their day-to-day work can
validate laboratory studies at scale, under realistic conditions, and
without observational bias. In this paper, we define field studies as
observational studies of software developers, conducted during their
daily work, with minimal or no interference to the developers' usual
activity.

In general, there are relatively few large scale research field
studies in software engineering, and there are no field studies of
feature location that we are aware of. While the difficulty in finding
developers who are willing to allow their activities to be monitored
by researchers is likely the greatest challenge to conducting field
studies, analyzing the resulting data can also pose challenges. The
main problem in analyzing field IDE data is interpreting high-level
developer behaviors from low-level logged events, without the
assistance of other sources of information (e.g. screen capture,
video, or questionnaires) typically available in laboratory
experiments. This problem is exacerbated by noise in field data, which
occurs as developers can often be interrupted during their daily work
or frequently switch between several maintenance tasks.


In this paper, we describe a field study of feature location that we
conducted to validate existing knowledge about this software
maintenance activity and discover other interesting usage patterns
exhibited by developers in the field. Specifically, we investigated
developer behavior along several dimensions, including the frequency
and types of code search tools used by developers, developer behavior
before and after search, types of queries issued by developers, and
complex feature location patterns. To analyze field data, we clustered
relevant events to form time-based sessions, which we qualitatively
determined were representative of developer behaviors. When possible,
we also used existing models of developer behavior during feature
location, constructed during controlled laboratory observations of
developers, as a starting point for analyzing field data.
 

The resulting field study consists of two datasets, one consisting of
all IDE actions by 67 developers at one company over a period of two
months, which enabled examination of the feature location process in
general, including tool types and complex actions. The other dataset
consists solely of logs gathered by an information retrieval based
code search tool, which includes 596 developers and 8,052 queries from
developers that downloaded the tool anonymously. The IDE dataset
provides information on many aspects of developer behavior, while the
search tool dataset provides information focused on behavior in code
search, commonly considered to be the initial activity performed by
developers during a feature location task.


The contribution of this paper is in both the data analysis approach
as well as the results from this analysis. The paper makes the
following main contributions based on the two field studies of
software developer activity. First, we confirm several commonly held
beliefs about how developers perform feature location in the field:

\begin{itemize}
\item Developers frequently and consistently use code search tools in
  the field, averaging several uses of these tools per workday.
\item Most queries consist of a single word.
\item Developers often reformulate their query, and they do so by
  adding, removing, or changing a term.
\item After starting with code search, developers' next step is most
  often navigation.
\end{itemize}

\noindent
Second, we discovered several unexpected patterns in the field data:

\begin{itemize}
\item Developers rarely use code search tools that are applicable to overly specific and limited information in the code.
\item Developers create many queries semi-automatically via copy/paste
  or editor text selection.
\item Developers tend to issue queries immediately after opening a
  software project (or solution\footnote{In this paper, we use the Visual Studio term solution to refer to a software project or a code base. A solution is a container consisting of one or more Visual
    Studio projects, which, in turn, contains a number of source code
    files.}).
\item When performing complex feature location tasks, developers tend
  to switch modalities several times (e.g., {\em search} to {\em navigate} to
  {\em search} to {\em navigate}).
\end{itemize}

\noindent
Both the confirmed and unexpected behaviors can shed light on
developer behavior during software maintenance, expose potential deficiencies and thus improvements in
laboratory experimental designs, and provide valuable suggestions to toolsmiths. 

The organization of this paper is as follows. Section 2 describes the
related work, while Section 3 describes the data sets we used and the
type of analyses we performed. In Section 4, we introduce the findings
of our field study, and, finally, in Section 5 we discuss the
implications of those findings.

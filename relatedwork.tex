\section{Related Work}

The related work for this paper can be grouped into two parts: (1)
feature location developer studies, and (2) large-scale field studies
in software maintenance. We are unaware of any previous large-scale
field study of feature location that intersects these two categories.

\subsection{Feature Location Developer Studies}

There have been a number of laboratory studies that have influenced
our understanding of how programmers perform feature location during
software maintenance. We outline the most influential of these here,
highlighting mental models of developer behavior during feature
location that were proposed by some of the studies. These mental
models describe the thought process of a developer performing feature
location, consisting of a set of steps mapping to a specific sequence
of actions performed within the IDE. We relied on such a model in our
field study to provide a basis for interpreting developer IDE actions.

%ko
An influential laboratory study of feature location was conducted by
Ko et al. (\cite{KoTSE2006}). The 10 developers (experienced graduate
students) in this study were observed while performing a number of
maintenance tasks in a 70 minute session on a Java paint application
that they were completely unfamiliar with. The 5 assigned maintenance
tasks were invented by the researchers and consisted of minor feature
requests and bug reports (e.g., a button in the UI does not work).
Periodic interruptions to the developers' work were simulated to more
closely resemble real-world development scenarios. The study reported
on the distribution of time on various parts of a maintenance task,
highlighting the importance of feature location and comprehension,
which dominated task time. The study further found that developers had
difficulty keeping track of all the relevant parts of the code base,
exposing opportunities for toolsmiths in maintaining task contexts for
software maintenance. Another contribution is the construction of a
high-level mental model of developers performing software maintenance
tasks, which was not very specifically instantiated with IDE actions.

%sillito
Sillito et al. conducted a study in which they used both code bases
that the participants were familiar and unfamiliar with
(\cite{sillito}). The part of the study that used an unfamiliar code
base consisted of 9 students that worked in pairs in 45 minute
sessions on 4 tasks selected from an open source project's history. The
part of the study that used a familiar code base included 16
industrial software engineers who were asked to think aloud while
working for 30 minutes on maintaining their own code bases. Voice and
video of the participants actions were recorded, coded and analyzied
with the goal of identifying a comprehensive list of program
comprehension questions that developers ask during software
maintenance tasks. The questions identified by this study can be
grouped into several categories, ranging from simple questions about
individual program elements to complex relationships between groups of
structurally interconnected methods.

%wang
A recent laboratory study performed by Wang et al. \citep{wangFl}
monitored 20 experienced (18 graduate students and 2 industry
developers) and 18 novice (3rd and 4th year undergraduate) developers
in performing 6 short feature location tasks in two 60 minute
sessions. The tasks were selected from the maintenance histories of
open source projects. While observing the experienced developers, the
researchers constructed a detailed mental model of feature location,
based on debugging, information retrieval based search, and structured
program navigation. The novice developers were taught this model,
after which a noticeable uptick in the quality of feature location
performed by these developers was observed. We use Wang et al.'s
mental model in our analysis, exploring how the abstract IDE actions
described in the model are realized by developers in the field.


%paragraph relating to this paper
With the exception of the second half of Sillito et al.'s study, all
of the studies described here used mostly student developers working
on unfamiliar code bases, although often choosing realistic tasks
extracted from software project histories. All of the studies used
short, fixed time intervals for the developers to perform the
tasks. In most cases, developers were observed in disruptive ways,
using video and audio monitoring and subsequent questionaires. In
this paper, we conduct a study that is different from previous studies
of feature location, which is based on IDE datasets collected
unobstrusively while developers worked at their own pace on their own
code bases.  Our study also consists of larger groups of developers monitored for
longer periods of time.

\subsection{Large-Scale Field Studies in Software Maintenance}

While relatively few large scale field studies have been conducted by
software maintenance researchers, all of the ones that have been
performed have uncovered interesting developer behaviors, which were
previously unreported by researchers.

%murphy-hill
Murphy-Hill et al. conducted a large-scale field study of refactoring
using several relevant datasets (\cite{murphy-hill}). One dataset
contained IDE data of 41 developers, with an average of 66 hours of
development time per developer. Another dataset used by Murphy-Hill et
al.  was the Eclipse Usage Collector dataset, which is extremely large
(13,000 developers), but only consists of frequency counts for each
command within the Eclipse IDE. The datasets used in our paper are
similar to Murphy-Hill et al.'s in that they combine both detailed
usage logs of a number of developers with a larger, but limited,
dataset reflecting a much larger set of developers in the field.

A study of Eclipse usage based on IDE data was conducted by Murphy et
al. \citep{murphy-eclipse}. The study used 41 developers' interaction
histories gathered using the Mylar Monitor Eclipse plugin
\citep{mylar-monitor}, which is a popular tool to gather usage data
in this IDE.  The study analyzed the frequency of use of a number of
Eclipse features, including refactoring, navigation and search, and
general commands and views (or windows), informing future development
of this popular software development environment. Similar to
Murphy-Hill et al., the dataset used by Murphy et al.'s study is large
but extremely limited in the depth of information it can provide.

There have been a few large-scale studies of code search behavior when
developers are searching on the web across a number of software
projects \citep{Bajracharya12}.  They find interesting results
including that developers use source code on the web for various
purposes, and use different forms of queries to express their
information needs, including natural language and names of code
entities they are aware of.  We have observed similar types of queries
in previous small-scale studies using Sando
\citep{paired-interleaving} as well as in the larger collection of
data analyzed in this paper.




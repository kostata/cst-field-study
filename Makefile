all: 
	pdflatex cst-field-study
	bibtex cst-field-study
	pdflatex cst-field-study

clean:
	$(RM) *.bbl *.inx *.blg *.dvi *.aux *.log *.ps *~

